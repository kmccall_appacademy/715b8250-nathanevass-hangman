class Hangman
  attr_reader :guesser, :referee, :board
  
  def initialize(players = default_computer_guesses)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @guesses = 10
  end 
  
  def default_computer_guesses
    {:guesser => ComputerPlayer.new, :referee => HumanPlayer.new}
  end 
  
  def default_human_guesses 
    {:referee => ComputerPlayer.new, :guesser => HumanPlayer.new}
  end 
  
  def play 
    setup 
    take_turn until over?
    handle_end 
  end 
  
  def decrease_guesses 
    @guesses -= 1
  end 
  
  def show_guesses 
    puts "There are #{@guesses} guesses left"
  end     
  
  def over?
    @board.none? {|char| char == nil} ||  no_guesses?
  end 
  
  def handle_end 
    if no_guesses?
      puts "No more guesses, referee wins!"
    else 
      puts "Guesser wins, guessed '#{word}' with #{@guesses} guesses remaining."
    end 
  end 
   
  def word 
    @board.join("")
  end 
   
  def no_guesses?
    @guesses == 0 
  end 
  
  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end 
  
  def update_board(indexes, letter)
    indexes.each do |i|
      @board[i] = letter 
    end  
  end 
  
  def handle_guesses(hit_indexes)
    decrease_guesses if hit_indexes.empty?  
  end 
  
  def display_board 
    @board.map  {|el| el.nil? ? "_" : el}.join
  end 
  
  def take_turn 
    show_guesses
    letter = @guesser.guess(@board)
    indexes = @referee.check_guess(letter)
    decrease_guesses if indexes.empty?
    update_board(indexes, letter)
    display_board 
    @guesser.handle_response(letter, indexes)
  end 
end

class HumanPlayer
  def initialize(dictionary = "")
    @dictionary = dictionary
  end 
  
  def candidate_words
    @dictionary
  end 
  
  def pick_secret_word
    puts "How many characters is your word?"
    gets.chomp.to_i 
  end 
  
  def check_guess(letter)
    puts "Is #{letter} in your word? y/n"
    answer = gets.chomp 
    if answer == 'y'
      return get_indexes(letter)
    else 
      return []
    end 
  end 
  
  def guess(board)
    puts "Guess a letter, there are #{board.length} characters"
    gets.chomp.downcase
  end 
  
  def get_indexes(letter)
    puts "Which letters matched the guess? eg. '1 2 4' for first, second and fourth"
    gets.chomp.split.map {|num| num.to_i - 1}
  end 
  
  def handle_response(letter, matching_indexes)
    puts "You guessed letter #{letter}"
  end 
  
  def register_secret_length(length) 
  end 
end

class ComputerPlayer
  attr_accessor :dictionary, :word  
  def initialize(dictionary)
    @dictionary = dictionary 
    @secret_word = ""
  end 
  
  def default_dictionary 
    File.read('dictionary.txt').split("\n")  
  end 
  
  def candidate_words
    @dictionary
  end 
    
  
  def pick_secret_word
    word = @dictionary.sample
    @secret_word = word 
    word.length 
  end 
  
  def check_guess(letter)
    (0...@secret_word.length).select {|i| @secret_word[i] == letter}
  end 

  
  def guess(board)
    frequencies = Hash.new(0)
    candidate_words.each do |word|
      word.chars do |char|
        frequencies[char] += 1 
      end 
    end 
    frequencies.reject! {|k, v| board.include? k }
    popular_pair = frequencies.max_by {|k, v| v}
    popular_pair[0] unless popular_pair == nil 
  end 
  
  def handle_response(letter, matching_indexes)
    if !matching_indexes.empty?
      candidate_words.select! do |word|
        matching_indexes.all? do |index|
          word[index] == letter 
        end &&
        matching_indexes.length == word.count(letter)
      end 
    else 
      candidate_words.reject! do |word|
        word.include?(letter)
      end 
    end 
  end 
  
  def register_secret_length(length)
    @dictionary.select! do |word|
      word.length == length 
    end 
  end 
end
